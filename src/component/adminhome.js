import React, { Component } from 'react';
import Adminsidemenu from './adminhomepage.js';
import Adminheadermenu from './adminheadermenu.js';

class Adminhome extends Component {
    render() {
      return (
        <div className="App">
            <Adminsidemenu/>
            <Adminheadermenu/>
        </div>
      );
    }
  }
  export default Adminhome;