import React, { Component } from 'react';
import {
    FaSortDown,
    FaUserCircle,
} from 'react-icons/fa';
class AdminHeadermenu extends Component {
    render() {
        return (
            <div className="headermenu">             
                <i className="positionY left right"><FaUserCircle /></i>
                <a class="header-active positionY" href="#"><FaSortDown /></a>
                <h5 className="positionText left right">Billy.B</h5>
            </div>
        );
    }
}
export default AdminHeadermenu;
