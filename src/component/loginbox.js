import React, { Component } from 'react';
import {Link} from 'react-router';

class Loginbox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        }
    }
    onUsernameChange(event) {
        this.setState({ username: event.target.value })
    }
    onPasswordChange(event) {
        this.setState({ password: event.target.value })
    }

    loginAPI() {
        // fetch(`https://127.0.0.1:8000/loginAPI`, {

        fetch(`https://127.0.0.1:8000/loginAPI?username=${this.state.username}&password=${this.state.password}`, {
            // method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                // 'Content-Type': 'application/json'
                'Cache-Control': 'no-cache'
            },
            // body: JSON.stringify({
            //     username: this.state.username,
            //     password: this.state.password,
            // })
        })
            .then(() => console.log('success'))
            .catch((error) => console.log(error));
    }

    render() {
        // console.log(this.state.username);
        // console.log(this.state.password);
        return (
            <div className="App Background">
                <div className="positionblock">
                    <div className="block">
                        <div className='container'>
                            <div>
                                <h1>STOCK</h1>
                                <input className="inputbox" type="text" placeholder="Username" value={this.state.username}
                                       onChange={this.onUsernameChange.bind(this)} /><br /><br />
                                <input className="inputbox" type="password" placeholder="Password" value={this.state.password}
                                       onChange={this.onPasswordChange.bind(this)} />
                            </div>
                            <div className="row flex-container">
                                <div className="flex1">
                                    <input type="checkbox" /><span className="font-md">Remember me</span>
                                </div>
                                <div className="flex1">
                                    <Link to="#" className="font-md">Forget Password</Link>
                                </div>
                            </div>
                            <div className="row flex-container">
                                <Link to="/login" className="login-position flex1 submitlogin hide-link-login"
                                onClick={()=> this.loginAPI()}> Login</Link>
                                <Link to="/signup" className="flex1 signuplogin  hide-link-signup">Sign up</Link>
                                {/* <a className="login-position flex1 submitlogin hide-link-login" href="/login">Login</a> */}
                                {/* <a className="flex1 signuplogin  hide-link-signup" href="/signup">Sign up</a> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Loginbox;