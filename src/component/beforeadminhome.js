import React, { Component } from 'react';
import Boxes from '../image/boxs.png';
import Oms from '../image/oms.png';
import {Link} from 'react-router';

class Beforeadminhome extends Component {
    render() {
        return (
            <div className="App Background">
                <div className="before-admin">
                    <Link to="/home" className="red-block"><img class="center" src={Boxes}/>
                    Inventoy Control</Link>
                    <Link to="" className="blue-block"><img class="center-oms" src={Oms}/>
                    Order Management System</Link>
                    {/* <a href="#" className="red-block">
                    <img class="center" src={Boxes}/>
                    Inventoy Control</a>
                    <a href="#" className="blue-block">
                    <img class="center-oms" src={Oms}/>
                    Order Management System</a> */}
                </div>
                <Link to="/" className="back-to-login">Back to Login</Link>
            </div>
        );
    }
}
export default Beforeadminhome;