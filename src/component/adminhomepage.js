import React, { Component } from 'react';
import {
    FaHome,
    FaUser,
    FaTh,
    FaChartPie,
    FaShoppingCart
} from 'react-icons/fa';

class Adminsidemenu extends Component {
    render() {
        return (
            <ul className="navbar">
                <li><a class="navbar-active text">STOCK</a></li>
                <li><a class="text sidemenu" href="#"><FaHome /><span class="space-span">Home</span></a></li>
                <li><a class="text sidemenu" href="#"><FaChartPie /><span class="space-span">Reports</span></a></li>
                <li><a class="text sidemenu" href="#"><FaTh /><span class="space-span">Products</span></a></li>
                <li><a class="text sidemenu" href="#"><FaUser /><span class="space-span">Users</span></a></li>
                <li><a class="text sidemenu" href="#"><FaShoppingCart /><span class="space-span">Orders</span></a></li>
            </ul>
        );
    }
}

export default Adminsidemenu;
