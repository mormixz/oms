import React, { Component } from 'react';
import {Link} from 'react-router';

class Register extends Component {
    render() {
        return (
            <div className="App Background">
                <div className="positionblock">
                    <div className="block-register">
                        <div className='container'>
                            <h1>REGISTER</h1>
                            <div className="row flex-container">
                                <div className="flex1">
                                    <input className="inputnamebox" type="text" placeholder="First name" />
                                </div>
                                <div className="flex1">
                                    <input className="inputlastnamebox" type="text" placeholder="Last name" />
                                </div>
                            </div>
                            <input className="inputbox" type="text" placeholder="Username" />
                            <input className="inputbox" type="email" placeholder="Email" />
                            <input className="inputbox" type="password" placeholder="Password" />
                            <input className="inputbox" type="password" placeholder="Confirm Password" />
                            <div className="submitsignup">
                                <Link to=" " className="hide-link-login">Sign up</Link>
                                {/* <a>Signup</a> */}
                            </div>
                            <p style={{ fontSize: 13 }}>Already have an account?<Link to="/">Sign in.</Link></p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Register;