import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Register from './component/register.js';
import Adminhome from './component/adminhome.js';
import Beforeadminhome from './component/beforeadminhome.js';
import * as serviceWorker from './serviceWorker';
import {Router,Route,Link,browserHistory} from 'react-router';

ReactDOM.render(
    <Router history={browserHistory}>
        <Route path="/" component={App}/>
        <Route path="/login" component={Beforeadminhome}/>
        <Route path="/signup" component={Register}/>
        <Route path="/home" component={Adminhome}/>
    </Router>,document.getElementById('root')
    );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
