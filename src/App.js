import React, { Component } from 'react';
import './App.css';
import Loginbox from './component/loginbox.js';
import Register from './component/register.js';
import Adminsidemenu from './component/adminhomepage.js';
import Adminheadermenu from './component/adminheadermenu.js';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Loginbox />
        {/* <Register/> */}
        {/* <Adminsidemenu/> */}
        {/* <Adminheadermenu/> */}
      </div>
    );
  }
}
export default App;
